class ComputerPlayer

  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves.each do |row, col|
      return [row, col] if wins?(row, col)
    end

    moves.sample
  end

  def wins?(row, col)
    board.grid[row][col] = mark
    if board.winner == mark
      board.grid[row][col] = nil
      true
    else
      board.grid[row][col] = nil
      false
    end
  end

  def moves
    nil_arr = []

    board.grid.each_with_index do |rows, idx1|
      rows.each_with_index do |moves, idx2|
        nil_arr << [idx1, idx2] if moves == nil
      end
    end

    nil_arr
  end
end
