class Board

  attr_reader :grid, :marks

  def initialize(grid=nil)
    @grid = grid
    @marks = [:X, :O]
    @grid = [
      [nil, nil, nil],
      [nil, nil, nil],
      [nil, nil, nil]
    ] if grid == nil
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark if empty?(pos)
  end

  def empty?(pos)
    grid[pos[0]][pos[1]] == nil
  end

  def winner
    res = nil

    if diagonal_win?(:X) || rows_win?(:X) || cols_win?(:X)
      res = :X
    elsif diagonal_win?(:O) || rows_win?(:O) || cols_win?(:O)
      res = :O
    end

    res
  end

  def rows_win?(piece)
    grid.each do |row|
      return true if row == [piece, piece, piece]
    end

    false
  end

  def cols_win?(piece)
    grid.transpose.each do |col|
      return true if col == [piece, piece, piece]
    end

    false
  end

  def diagonal_win?(piece)
    left_diagonal = [grid[0][0], grid[1][1], grid[2][2]]
    right_diagonal = [grid[0][2], grid[1][1], grid[2][0]]
    winner = [piece, piece, piece]

    return true if left_diagonal == winner || right_diagonal == winner

    false
  end

  def cats_game?
    grid.all? do |rows|
      rows.all? do |pieces|
        pieces != nil
      end
    end
  end

  def over?
    winner || cats_game?
  end
end
