class HumanPlayer

  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Please enter where you want to place your piece:"
    puts "ex: 0, 0"
    user_move = gets.chomp
    user_move.split(',').map! {|el| el.to_i}
  end

  def display(board)
    row0 = "0 |"
    (0..2).each do |col|
      row0 << (board.empty?([0, col]) ? "   |" : " " + board.grid[0][col].to_s + " |")
    end
    row1 = "1 |"
    (0..2).each do |col|
      row1 << (board.empty?([1, col]) ? "   |" : " " + board.grid[1][col].to_s + " |")
    end
    row2 = "2 |"
    (0..2).each do |col|
      row2 << (board.empty?([2, col]) ? "   |" : " " + board.grid[2][col].to_s + " |")
    end


  puts "    0   1   2  "
  puts "  |-----------|"
  puts row0
  puts "  |-----------|"
  puts row1
  puts "  |-----------|"
  puts row2
  puts "  |-----------|"
  end
end
